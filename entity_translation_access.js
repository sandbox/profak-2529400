/**
 * @file
 * Javascript logic for module.
 */

(function ($) {
  "use strict";
  $(document).ready(function () {
    $('<a/>', {
      class: 'invert-selection',
      href: '#',
      text: Drupal.t('Inverse selection')
    }).appendTo('.edit-entity-translation-access-control .fieldset-wrapper');

    $('a.invert-selection').click(function (event) {
      event.preventDefault();
      var $fs = $(this).parent('.fieldset-wrapper');
      $fs.find('.form-type-checkbox input').each(function () {
        if ($(this).is(':checked')) {
          $(this).removeAttr('checked');
        }
        else {
          $(this).attr('checked', 'checked');
        }
      });
    });
  });
})(jQuery);
