CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
When you have entities translated using entity translation sometimes it's
very handy to deny access to entity on specific language especially due to a
language inheritance provided by entity translation. Here comes this
module - it helps you to hide entity per language, so you will be able to
have "All languages" path alias, but on denied languages it won't show up.

REQUIREMENTS
------------
This module requires the following modules:
 * Entity Translation (https://drupal.org/project/entity_translation)

INSTALLATION
------------
* Install as you would normally install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

* You can edit node that has entity translation enabled and see tab
  in additional settings "Select languages to deny access" in node form.

CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no configuration.

MAINTAINERS
-----------
Current maintainers:
 * Andrey Khromyshev (profak) - https://drupal.org/u/profak

This project has been sponsored by:
 * Acronis International GmbH
   Acronis sets the standard for New Generation Data Protection through its
   backup, disaster recovery, and secure access solutions.
   Powered by the AnyData Engine and set apart by its image technology,
   Acronis delivers easy, complete and safe backups of all files,
   applications and OS across any environment—virtual, physical,
   cloud and mobile.

   Founded in 2003, Acronis protects the data of over 5 million consumers
   and 500,000 businesses in over 130 countries.
   With its more than 50 patents, Acronis’ products have been named
   best product of the year by Network Computing,
   TechTarget and IT Professional and cover a range of features,
   including migration, cloning and replication.

   For additional information, please visit www.acronis.com.
   Follow Acronis on Twitter: http://twitter.com/acronis.
