<?php
/**
 * @file
 * API for Entity Translation Access.
 */

/**
 * Adds ability to process menu item in custom module.
 *
 * This hook will help other modules to change menu link,
 * because hook_translated_menu_link_alter() implemented with this module will
 * run after all modules.
 *
 * @param mixed $item
 *   Menu item.
 * @param array $data
 *   Related entity info: entity_type, entity_id.
 */
function hook_entity_translation_access_menu_link_alter(&$item, array $data) {
}
