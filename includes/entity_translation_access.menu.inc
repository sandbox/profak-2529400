<?php
/**
 * @file
 * Menu handling.
 */

/**
 * Implements hook_translated_menu_link_alter().
 */
function entity_translation_access_translated_menu_link_alter(&$item, $map) {
  $data = array();

  // We don't want show disabled in menu interface.
  // menu_overview_form has already UI to show hidden languages.
  if (arg(0) == 'admin' || drupal_is_cli()) {
    return;
  }

  if (preg_match('/^[a-z]+\/\d+$/i', $item['link_path'])) {
    list($entity_type, $entity_id) = array_pad(explode('/', $item['link_path']), 2, NULL);
    $data = array(
      'entity_id'   => $entity_id,
      'entity_type' => $entity_type,
    );

    if ($entity_id) {
      if (entity_translation_access_language_status($entity_id, $entity_type) || entity_translation_access_menu_denied($item)) {
        $item['hidden'] = 1;
      }
    }
  }
  elseif (entity_translation_access_menu_denied($item)) {
    $item['hidden'] = 1;
  }
  drupal_alter('entity_translation_access_menu_link', $item, $data);
}

/**
 * Hooks menu overview form.
 */
function entity_translation_access_form_menu_overview_form_alter(&$form, &$form_state, $form_id) {
  foreach (element_children($form) as $name) {
    $item_hidden_in_menu = FALSE;
    $entity_hidden_for_languages = FALSE;

    if (strpos($name, 'mlid') !== FALSE) {

      if (!empty($form[$name]['#item']['options']['menu_access'])) {
        $item_hidden_in_menu = implode(' | ', $form[$name]['#item']['options']['menu_access']['languages']);
      }

      if (!empty($item_hidden_in_menu)) {
        $form[$name]['title']['#markup'] .= '<div style="text-align: right;">' . t('<b style="color: #006400;">Menu item</b> is hidden for next languages:<br/> <span style="font-size: 10px;">%languages</span>', array('%languages' => $item_hidden_in_menu)) . '</div>';
      }

      if (entity_translation_access_check_is_entity_link($form[$name]['#item']['link_path'])) {
        list($entity_type, $entity_id) = array_pad(explode('/', $form[$name]['#item']['link_path']), 2, NULL);

        // We call alter functions here, because we
        // can map entity_type/entity_id somehow.
        drupal_alter('entity_translation_access_entity_info', $entity_type, $entity_id);

        $entity = entity_load($entity_type, array($entity_id));
        if (!empty($entity)) {
          $entity = $entity[$entity_id];
          $entity_hidden_for_languages = '';
          if (!empty($entity->entity_denied_languages)) {
            $entity_hidden_for_languages = implode(' | ', $entity->entity_denied_languages);
          }
        }

        if (!empty($entity_hidden_for_languages)) {
          $form[$name]['title']['#markup'] .= '<hr/><div style="text-align: right;">' . t('<span style="color: #cd853f;">Linked <b>%entity</b></span> hidden for next languages:<br/> <span style="font-size: 10px;">%languages</span>', array(
              '%entity'    => $entity_type,
              '%languages' => $entity_hidden_for_languages,
            )) . '</div>';
        }
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Adds entity translation access form for menu item.
 */
function entity_translation_access_form_menu_edit_item_alter(&$form, &$form_state) {
  $menu_item = $form['original_item']['#value'];

  $selected_languages = array();
  if (isset($menu_item['options']['menu_access'])) {
    $selected_languages = $menu_item['options']['menu_access']['languages'];
  }

  entity_translation_access_denied_languages_form($form, $form_state, $selected_languages);
  array_unshift($form['#submit'], 'entity_translation_access_menu_languages_form_submit');
}

/**
 * Check menu item translation access.
 *
 * @return bool
 *   Returns access status for menu item.
 */
function entity_translation_access_menu_denied($item) {
  global $language;
  if (isset($item['options']['menu_access']['languages'][$language->language])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Check value to be a valid entity internal link.
 *
 * We have to be sure, that we handle entities paths, that have routes like:
 * entity_name/entity_id.
 */
function entity_translation_access_check_is_entity_link($value) {
  return preg_match('/^[a-z]+\/\d+$/i', $value);
}
