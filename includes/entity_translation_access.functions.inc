<?php

/**
 * @file
 * Translation access model.
 */

/**
 * Return language access status for given entity of type.
 *
 * @return bool
 *   Returns status per language.
 */
function entity_translation_access_language_status($entity_id, $entity_type = 'node', $language_code = NULL) {
  if (!$language_code) {
    global $language;
    $language_code = $language->language;
  }
  $table = &drupal_static(__FUNCTION__, array());
  if (!$table) {
    $rows = db_select('entity_translation_access', 'ta')
      ->fields('ta', array('entity_id', 'entity_type', 'language'))
      ->execute();
    foreach ($rows as $row) {
      $table[$row->entity_type][$row->entity_id][$row->language] = $row->language;
    }
  }
  $status = FALSE;
  if (isset($table[$entity_type][$entity_id][$language_code])) {
    $status = TRUE;
  }
  return $status;
}

/**
 * Check denied languages for entity of entity_type.
 *
 * @return mixed
 *   Returns denied languages per entity.
 */
function entity_translation_access_languages_denied($entity_id, $entity_type = 'node') {
  $languages = &drupal_static(__FUNCTION__, array());
  if (!isset($languages[$entity_type][$entity_id])) {
    $lang = entity_translation_access_get_entity_denied_languages($entity_id, $entity_type);
    $languages[$entity_type][$entity_id] = $lang;
  }
  return $languages[$entity_type][$entity_id];
}

/**
 * Saves/updates denied languages for provided entity.
 */
function entity_translation_access_languages_save($entity, $entity_type, $languages) {
  $entity_id = entity_id($entity_type, $entity);

  if (!empty($languages['to_save'])) {
    $insert_query = db_insert('entity_translation_access')->fields(array(
      'entity_id',
      'entity_type',
      'language',
    ));
    foreach ($languages['to_save'] as $language) {
      if ($language) {
        $insert_query->values(array(
          'entity_id'   => $entity_id,
          'entity_type' => $entity_type,
          'language'    => $language,
        ));
      }
    }
    $insert_query->execute();
  }

  if (!empty($languages['to_delete'])) {
    $delete_query = db_delete('entity_translation_access');
    $delete_query->condition('entity_id', $entity_id);
    $delete_query->condition('entity_type', $entity_type);
    $delete_query->condition('language', $languages['to_delete'], 'IN');
    $delete_query->execute();
  }

}

/**
 * Get list of all languages per entity of entity_type.
 */
function entity_translation_access_get_entity_denied_languages($entity_id, $entity_type = 'node') {
  $query = db_select('entity_translation_access', 'ta')
    ->fields('ta', array('language'))
    ->condition('entity_id', $entity_id)
    ->condition('entity_type', $entity_type)
    ->execute()
    ->fetchAllKeyed(0, 0);
  return $query;
}
