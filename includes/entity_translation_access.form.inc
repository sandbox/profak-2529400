<?php
/**
 * @file
 * Form function for module.
 */

/**
 * Basic entity translation form.
 */
function entity_translation_access_denied_languages_form(&$form, &$form_state, $selected_languages = array()) {
  $options = locale_language_list('name');

  $form['entity_translation_access'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Select languages to hide'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
    '#group'       => 'additional_settings',
    '#weight'      => 80,
    '#tree'        => TRUE,
    '#attributes'  => array('class' => array('entity-denied-languages', 'edit-entity-translation-access-control')),
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'entity_translation_access') . '/entity-translation-access.js',
      ),
      'css' => array(
        drupal_get_path('module', 'entity_translation_access') . '/entity_translation_access.css',
      ),
    ),
  );

  $form['entity_translation_access']['languages'] = array(
    '#type'          => 'checkboxes',
    '#options'       => $options,
    '#default_value' => $selected_languages,
    '#suffix'        => '<a class="invert-selection">' . t('Inverse selection') . '</a>',
  );

  return $form;
}

/**
 * Entity translation access submit handler for menu item.
 *
 * Saves denied languages for menu.
 */
function entity_translation_access_menu_languages_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['entity_translation_access']['languages'])) {
    foreach ($form_state['values']['entity_translation_access']['languages'] as $key => $value) {
      if (empty($value)) {
        unset($form_state['values']['entity_translation_access']['languages'][$key]);
      }
    }
    $form_state['values']['options']['menu_access'] = $form_state['values']['entity_translation_access'];
  }
}
