/**
 * @file
 * Javascript logic for fieldset.
 */

(function ($) {
  "use strict";
  Drupal.behaviors.entityTranslationAccess = {
    attach: function (context) {
      $('fieldset.entity-denied-languages', context).drupalSetSummary(function (context) {
        var $selected = [];
        $('input[type="checkbox"]:checked', context).each(function (index) {
          $selected.push('<span class="denied-language">' + $(this).next().text().trim() + '</span>');
        });
        return $selected;
      });

      $('.invert-selection', context).click(function (event) {
        event.preventDefault();
        var $fs = $(this).parent('.fieldset-wrapper');
        $fs.find('.form-type-checkbox input').each(function () {
          if ($(this).is(':checked')) {
            $(this).removeAttr('checked');
          }
          else {
            $(this).attr('checked', 'checked');
          }
        });
      });

    }
  };
})(jQuery);
